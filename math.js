function marksSum(a)
{
      let total = 0
      a.forEach( (n) => {
           total = total + n
      } )
     return total
}

function avg(total,n){
    let res = total / n
    return res
}

let marks =  [49,49,239,53,494]
let total = marksSum(marks )
let res = avg(total,marks.length)
console.log(res)