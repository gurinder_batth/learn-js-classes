let todo = []
const task = document.getElementById("task")
const message = document.getElementById("message")
const table = document.getElementById("table")
const updateform = document.getElementById("updateform")
const addform = document.getElementById("addform")
const taskinput = document.getElementById("task_u")
const updated_index = document.getElementById("updated_index")
const addTodo = () => {
      if(task.value.length < 1){
          hideMessage()
          return ""
      }
      let item = { name:task.value }
      todo.push(item)
      task.value = ""
      displayMessage()
      displayTable()
}

const displayMessage = () => {
      message.style.display = "block"
}

const hideMessage = () => {
       message.style.display = "none"
}

const displayTable = () => {
      let data = ""
      todo.forEach( (item,index) => {
           let tr = `<tr> 
                <td> ${index+1} </td>
                <td> ${item.name} </td>
                <td> <button class='btn btn-danger' onclick='del(${index})'>Del</button> </td>
                <td> <button class='btn btn-success' onclick="edit(${index},'${item.name}')">Edit</button> </td>
           </tr>`
           data += tr
      } )
      table.innerHTML = data

}

function del(i){
      todo.splice(i,1)
      displayTable()
}
function edit(i,taskname){
        updateform.style.display = "block" 
        addform.style.display = "none" 
        taskinput.value = taskname
        updated_index.value = i
}

function updateTodo()
{
      let new_title = taskinput.value
      let index = updated_index.value
      todo[index].name = new_title
      displayTable()
      updateform.style.display = "none" 
      addform.style.display = "block" 
}