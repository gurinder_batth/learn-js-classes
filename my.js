let is_bottom_run = false
function changeColors(timer)
{
    const box = document.getElementsByClassName("box")
    for(let i = 0; i < box.length ; i++){
        if(i%2 == 0){
           if(timer % 2  == 0){
              box[i].style.backgroundColor = "red"
           }
           else{
             box[i].style.backgroundColor = "green"
           }
        }
    }
}

function colorChangeBox(){
    let timer =  0
    let myinterval =  setInterval( () => {        
            changeColors(timer)
            timer++
            if(timer == 4){
                clearInterval(myinterval)
            }
    } , 500 )
}

function stop(){
      is_bottom_run = true
}