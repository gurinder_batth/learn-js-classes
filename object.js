let s2 = { name:"jerry" , id:2 }

let students = [
    { name:"Tom" , id:1  } ,
    s2 ,
    { name:"Abc" , id:3 }
]

let teachers = [
    {   
        name:"John" , 
        phones: [ 
            { phone:"1234567" } ,
            { phone:"1245456" } ,
       ] 
    } ,
    {   
        name:"Viskas" , 
        phones: [ 
            { phone:"23433455" } ,
            { phone:"3243564756" } ,
       ] 
    } ,
]

let data = { data:students , teachers:teachers }

// console.log(data)

// data.teachers.forEach( (teacher) => {
//         teacher.phones.forEach( (phones)  => {
//              console.log(phones.phone)
//         })
// })

// data.data.forEach( (student) => {
//      console.log(student.name)
// })data.teachers.forEach( (teacher) => {
//         teacher.phones.forEach( (phones)  => {
//              console.log(phones.phone)
//         })
// })



let bigdata = [  [ 10 , 20 ] , [ 20 , 85 ] , [ 57 , 85 ] ]

let sum = 0
for(let first of bigdata ){
   for(let second of first){
      sum = sum + second
   }
}

console.log(sum)

for (let index = 0; index < bigdata.length; index++) {
   for(let j = 0 ; j < bigdata[index].length;j++){
      console.log(bigdata[index][j])
   }
}
let sum2 = 0
bigdata.forEach( (first) => {
        first.forEach((second) => {
            sum2 = sum2 + second
        })
} )
console.log(sum2)

// let sum = 0
// bigdata.forEach( function(val){
//     sum = sum + val
// })
// console.log(sum)